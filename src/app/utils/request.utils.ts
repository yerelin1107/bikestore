import { HttpParams } from '@angular/common/http';
import { Key } from 'protractor';

export const createRequestParams =(req?: any): HttpParams  => {
let options: HttpParams = new HttpParams();
if(req){
    Object.keys(req).forEach(key => {
        options = options.set(key, req[key] )

    })

}
return options;
}

//array
let datos = [1,2,3,4,5];//for

let dataClient = {//
    name:'Carlos',
    age: 23
}
dataClient['name']//carlos