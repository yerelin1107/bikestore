import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { Authority } from './auth/auth-shared/constants/authority.constants';
import { UserRouteAccesGuard } from './auth/guards/user-route-acces.guard';
import { AccesDeniedComponent } from './auth/acces-denied/acces-denied.component';
import { SalesModule } from './components/sales/sales.module';

const routes: Routes = [
  {
    path: 'dashboard',
    data: {
      authorities: [Authority.ADMIN]
    },
    canActivate: [UserRouteAccesGuard],
    loadChildren: () => import('../app/dashboard/dashboard.module')
      .then(module => module.DashboardModule)
  },
  {
    path: '',
    redirectTo: 'auth/login',
    pathMatch: 'full'
  },
  {
    path: 'auth/login',
    component: LoginComponent
  },
  {
    path: 'accessdenied',
    component: AccesDeniedComponent
  },
  {
    path: 'bikes',
    loadChildren: () => import('../app/components/bikes/bikes.module')
      .then(module => module.BikesModule)
  },
  {
    path: 'sales',
    loadChildren: () => import('../app/components/sales/sales.module')
      .then(module => module.SalesModule)
  },
  {
    path: 'clients',
    loadChildren: () => import('../app/components/clients/clients.module')
      .then(module => module.ClientsModule)
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
