export interface IBikeWithClient {
    price?: number;
    serial?: string;
    model?: string;
    status?: boolean;

    name?: string;
    documentNumber?: string;
}
