import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IReto } from './ireto';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class RetoService {

  constructor(private http: HttpClient) { }


  public getRetoByPostId(postId: string): Observable<IReto>{
    let params = new HttpParams();
    params = params.append('postId', postId);
    console.warn('PARAMS', params);
    return this.http.get<IReto>(`${environment.END_POINT}/comments`,{params: params})
    .pipe(map(res => {
      return res;
    }));
  }

}
