import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms'

import { RetoRoutingModule } from './reto-routing.module';
import { BuscadorComponent } from './buscador/buscador.component';


@NgModule({
  declarations: [BuscadorComponent],
  imports: [
    CommonModule,
    RetoRoutingModule,
    ReactiveFormsModule
  ]
})
export class RetoModule { }
