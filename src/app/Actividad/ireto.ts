export interface IReto {
    postId: String;
    id: number;
    name: String;
    email: String;
    body: String
}
