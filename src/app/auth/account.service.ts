import { Injectable } from '@angular/core';
import { ReplaySubject, Observable, of } from 'rxjs';
import { Account } from './auth-shared/models/account.model';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { catchError, shareReplay, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  private authenticationState = new ReplaySubject<Account | null>(1);
  private userIdentity:Account | null = null;
  private accountCache?: Observable<Account | null>;
  
  constructor(
    private http: HttpClient, private router: Router
  ) { }
    identity(force?: boolean): Observable<Account | null> {
    if(this.accountCache || force || !this.isAuthenticated()){
      this.accountCache = this.fetch().pipe(catchError(() => {
      return of (null);  
      }), tap((account: Account | null)=>{
     this.authenticate(account);
     if (account) {
       this.router.navigate(['/dashboard']);
     }   

       }), shareReplay());
    } 
    return this.accountCache; 
    }
    authenticate(account: Account | null): void{
    this.userIdentity = account;      
    }
    isAuthenticated(): boolean { 
      return this.userIdentity !== null;
    }
    getAuthenticationState(): Observable<Account | null> {
      return this.authenticationState.asObservable();
    }
  
  /**
   *  autenticacion de roles
   * @param authorities 
   */

  hasAnyAuthority(authorities: string[] | string): boolean {
    if (!this.userIdentity || !this.userIdentity.authorities) {
      return false;
    }
    if (!Array.isArray(authorities)) {
      authorities = [authorities]
    }
  
    return this.userIdentity.authorities.some((authority: string) => authorities.includes(authority));

  }
    private fetch(): Observable<Account>{
      return this.http.get<Account>(`${environment.END_POINT}/api/account`);
    }
    
}

