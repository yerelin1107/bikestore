import { Injectable } from '@angular/core';
import { ICredentials } from './auth-shared/models/credentials';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  public login(credentials: ICredentials): Observable<any> {
    const data: ICredentials = {
      username: credentials.username,
      password: credentials.password,
      rememberMe: credentials.rememberMe
    };

    /*Process to verify credentials of user and save data in localStorage or sessionStorage*/
    return this.http.post<any>(`${environment.END_POINT}/api/authenticate`, data, {observe: 'response'})
    .pipe(map(res => {
      const bearerToken = res.headers.get('Authorization');
      console.log('VER TOKEN', bearerToken);
      if (bearerToken && bearerToken.slice(0, 7) === 'Bearer ') {
        const jwt = bearerToken.slice(7, bearerToken.length);
        this.storeAuthenticationToken(jwt, credentials.rememberMe);
        /*jwt is a string of contain token send backend*/
        return jwt;
      }
    }));
  }

  /*Closed session and delete token of localStorage and sessionStorage*/
  public logout(): Observable<any> {
    return new Observable(observe => {
      localStorage.removeItem('token');
      sessionStorage.removeItem('token');
      observe.complete();
    });
  }

  /*Save in the sesions of user (space in the navegator), this token.*/
  private storeAuthenticationToken(jwt: string, rememberMe: boolean): void {
    if (rememberMe === true) {
      /*localstorage is a microdatabase save information persistence in variable "token"*/
      localStorage.setItem('token', jwt);
    } else {
      /*sessionStorage delete information whenever the browser is closed */
      sessionStorage.setItem('token', jwt);
    }
  }
}


