import { Injectable } from '@angular/core';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';
import { Account } from '../auth-shared/models/account.model';
import { ICredentials } from '../auth-shared/models/credentials';
import { flatMap } from 'rxjs/operators';
import { AccountService } from '../account.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private authService: AuthService,
    private accountService:AccountService
  ) { }
 public login(credentials:ICredentials): Observable<Account | null>{
  return this.authService.login(credentials).pipe(flatMap(()=> this.accountService.identity())); 
 }
}
