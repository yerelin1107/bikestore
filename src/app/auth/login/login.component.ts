import { Component, OnInit } from '@angular/core';
import { Credentials, ICredentials } from '../auth-shared/models/credentials';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.styl']
})
export class LoginComponent implements OnInit {

  loginForm = this.fb.group({
    username: [''],
    password: [''],
    rememberMe: [false]
  });

  constructor(
    private fb: FormBuilder,
    private loginService: LoginService,
    private router: Router
  ) { }

  ngOnInit(): void {

  }

  login(): void {
    console.warn('DATA FORM USER', this.loginForm.value);
    /*Variable "credentials" of type interface ICredentials
    that will be an instance of the class Credentials*/
    const credentials: ICredentials = new Credentials();

    credentials.username = this.loginForm.value.username;
    credentials.password = this.loginForm.value.password;
    credentials.rememberMe = this.loginForm.value.rememberMe;

    this.loginService.login(credentials)
      .subscribe((res: any) => {
        console.warn('DATA LOGIN CONTROLLER - Ok Login ', res);
        this.accessTrue();
      }, (error: any) => {
        if (error.status === 401) {
          console.warn('User or Password invalid');
          // this.accessDenied();
        }
      });
  }

  accessTrue(): void {
    setTimeout(() => {
      
    }, 3000);
  }
  /*accessDenied(): void {
    setTimeout(() => {
      this.router.navigate(['../../app-routing.module.ts/access-denied']);
    }, 3000);
  }*/
}
