import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { MainBikesComponent } from '../components/bikes/main-bikes/main-bikes.component';
import { Authority } from '../auth/auth-shared/constants/authority.constants';
import { UserRouteAccesGuard } from '../auth/guards/user-route-acces.guard';
import { BikesCreateComponent } from '../components/bikes/bikes-create/bikes-create.component';
import { BikesUpdateComponent } from '../components/bikes/bikes-update/bikes-update.component';
import { BikesListComponent } from '../components/bikes/bikes-list/bikes-list.component';
import { MainClientsComponent } from '../components/clients/main-clients/main-clients.component';
import { ClientsCreateComponent } from '../components/clients/clients-create/clients-create.component';
import { ClientsFormsComponent } from '../components/clients/clients-forms/clients-forms.component';
import { ClientsListComponent } from '../components/clients/clients-list/clients-list.component';
import { MainSalesComponent } from '../components/sales/main-sales/main-sales.component';
import { SalesListComponent } from '../components/sales/sales-list/sales-list.component';
import { CatalogoComponent } from '../components/sales/catalogo/catalogo.component';
import { ClientsUpdateComponent } from '../components/clients/clients-update/clients-update.component';


const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'bikes',
        component: MainBikesComponent,
        data: {
          authorities: [Authority.ADMIN, Authority.CLIENT, Authority.USER]
        },
        canActivate: [UserRouteAccesGuard],
        children: [
          {
            path: 'bikes-create',
            component: BikesCreateComponent
          },
          {
            path: 'bikes-list',
            component: BikesListComponent
          },
          {
            path: 'bikes-update/:id',
            component: BikesUpdateComponent
          },


          {
            path: 'clients',
            component: MainClientsComponent,
            data: {
              authorities: [Authority.ADMIN, Authority.CLIENT, Authority.USER]
            },
            canActivate: [UserRouteAccesGuard],
            children: [

              {
                path: 'clients-create',
                component: ClientsCreateComponent
              },
              {
                path: 'clients-list',
                component: ClientsListComponent
              },
              {
                path: 'clients-forms',
                component: ClientsFormsComponent
              },

              {
                path: 'clients-update/:id',
                component: ClientsUpdateComponent
              },

              {
                path: 'sales',
                component: MainSalesComponent,
                data: {
                  authorities: [Authority.ADMIN, Authority.CLIENT, Authority.USER]
                },
                canActivate: [UserRouteAccesGuard],
                children: [

                  {
                    path: 'sales-list',
                    component: SalesListComponent
                  },
                  {
                    path: 'catalogo',
                    component: CatalogoComponent
                  },

                ]
              }
            ]
          }
        ]
      }
    ]
  }
  
];
      
@NgModule({
      imports: [RouterModule.forChild(routes)],
      exports: [RouterModule]
    })
export class DashboardRoutingModule { }
