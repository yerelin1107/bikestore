import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.styl']
})
export class NavbarComponent implements OnInit {

  constructor(
    private authService:AuthService
  ) { }

  ngOnInit() {
  }
  LogOut():void {
    this.authService.logout()
    .subscribe(res => {
    console.warn('mensaje');
})
  }
}
