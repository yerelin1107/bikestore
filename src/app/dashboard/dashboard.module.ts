import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { MainComponent } from './main/main.component';
import { NavbarComponent } from './navbar/navbar.component';
import { AuthSharedModule } from '../auth/auth-shared/auth-shared.module';
import { BikesModule } from '../components/bikes/bikes.module';
import { SalesModule } from '../components/sales/sales.module';
import { ClientsModule } from '../components/clients/clients.module';


@NgModule({
  declarations: [MainComponent, NavbarComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    AuthSharedModule,
    BikesModule,
    SalesModule,
    ClientsModule
  ]
})
export class DashboardModule { }
