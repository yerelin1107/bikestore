import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AccesDeniedComponent } from './auth/acces-denied/acces-denied.component';
import { LoginComponent } from './auth/login/login.component';
import { AuthInterceptor } from './auth/guards/auth.guard';
import { ReactiveFormsModule } from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AccesDeniedComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule
 
  ],
  providers: [
  
    {
      provide: HTTP_INTERCEPTORS,
      useClass:AuthInterceptor,
      multi:true

    } 
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }