import { Component, OnInit } from '@angular/core';
import { SalesService } from '../sales.service';
import { BikesService } from '../../bikes/bikes.service';
import { IBike } from '../../bikes/interfaces/bike';
import { ISale } from '../sales';
import { ClientsService } from '../../clients/clients.service';

@Component({
  selector: 'app-catalogo',
  templateUrl: './catalogo.component.html',
  styleUrls: ['./catalogo.component.styl']
})
export class CatalogoComponent implements OnInit {
  bikesList: IBike[];
  salesListCar: ISale[] = [];
  clientSelectedTemp: any;

  
  constructor(
    private salesService: SalesService,
    private bikesService: BikesService,
    private clientsService:ClientsService
    ) { 
    }

  ngOnInit(): void {
    this.bikesService.queryBikes()
    .subscribe(res => {
      this.bikesList = res;
    })
  }

  selectedClient
  
  addToCar(bike: IBike): void {
    console.warn('Selected ',bike);
    this.salesListCar.push({
      clientId: this.clientSelectedTemp.id,
      bikeId: bike.id,
      bikeSerial: bike.serial,
      bike: bike
    });
  }
}