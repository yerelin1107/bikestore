import { IBike } from '../bikes/interfaces/bike';
import { IClient } from '../clients/clients';

export interface ISale {
    id?: number,
    date?: Date,
    clientId?: number,
    clientName?: String,
    bikeId?: number,
    bikeSerial?: String,
    client?: IClient,
    bike?: IBike,
}


