import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ISale } from './sales';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SalesService {

  constructor(private http:HttpClient) { }

  public query(): Observable<ISale[]>{
    return this.http.get<ISale[]>(`${environment.END_POINT}/api/sales`)
    .pipe(map(res =>{
      return res;

    }));
  }
}
