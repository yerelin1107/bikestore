import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ClientsService } from '../clients.service';


@Component({
  selector: 'app-clients-forms',
  templateUrl: './clients-forms.component.html',
  styleUrls: ['./clients-forms.component.styl']
})
export class ClientsFormsComponent implements OnInit {


clientFormGroup: FormGroup;

  constructor( private formBuilder: FormBuilder, 
    private clientsService: ClientsService,  ) 
  
  { 
    this.clientFormGroup = this.formBuilder.group({
      name: ['', Validators.compose([Validators.required, Validators.maxLength(6)])],
      document: ['', Validators.compose([Validators.required, Validators.maxLength(10)])], 
      email: ['', Validators.compose([Validators.email, Validators.required, Validators.maxLength(12)])],
      phoneNumber: ['',Validators.compose([Validators.required, Validators.maxLength(12)])],
      documentType: ['',Validators.compose([Validators.required, Validators.maxLength(8)])]
    })
   }

   ngOnInit() {

  }
   saveClient(){
    console.log('Datos', this.clientFormGroup.value)
    this.clientsService.saveClient(this.clientFormGroup.value)
    .subscribe(res =>{
      console.log('Save ok', res);
    }, error =>{
      console.error('Error', error);
    });
   }

   updateClient(){
     console.log('New Datos', this.clientFormGroup.value)
     this.clientsService.updateClient(this.clientFormGroup.value)
     .subscribe(res =>{
       console.log('Update Ok', res);
     }, error =>{
       console.error('error', error);
     });
   }
   


}
