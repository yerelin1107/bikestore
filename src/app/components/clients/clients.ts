export interface IClient {
    id?: number;
    name?: String;
    document?: string;
    email?: String;
    phoneNumber?: String;
    documentType?: String;
    image?: string;
    imageContentType?: string;
}
