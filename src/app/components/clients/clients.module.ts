import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClientsRoutingModule } from './clients-routing.module';
import { ClientsListComponent } from './clients-list/clients-list.component';
import { ClientsFormsComponent } from './clients-forms/clients-forms.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ClientsUpdateComponent } from './clients-update/clients-update.component';
import { MainClientsComponent } from './main-clients/main-clients.component';
import { ClientsCreateComponent } from './clients-create/clients-create.component'

@NgModule({
  declarations: [ClientsListComponent, ClientsFormsComponent, ClientsUpdateComponent, MainClientsComponent, ClientsCreateComponent],
  imports: [
    CommonModule,
    ClientsRoutingModule,
    ReactiveFormsModule
  ]
})
export class ClientsModule { }
