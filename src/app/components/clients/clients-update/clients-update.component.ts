import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ClientsService } from '../clients.service';
import { ActivatedRoute, Router } from '@angular/router';
import { IClient } from '../clients';
import { Route } from '@angular/compiler/src/core';

@Component({
  selector: 'app-clients-update',
  templateUrl: './clients-update.component.html',
  styleUrls: ['./clients-update.component.styl']
})
export class ClientsUpdateComponent implements OnInit {

  clientFormGroup: FormGroup;

  constructor( private formBuilder: FormBuilder, private clientsService: ClientsService,
    private activeRoute: ActivatedRoute, private router: Router ) 
     { 

    this.clientFormGroup = this.formBuilder.group({
      id: [''],
      name: ['', Validators.compose([Validators.required, Validators.maxLength(30)])],
      document: ['', Validators.compose([Validators.required, Validators.maxLength(30)])], 
      email: ['', Validators.compose([Validators.email, Validators.required, Validators.maxLength(32)])],
      phoneNumber: ['',Validators.compose([Validators.required, Validators.maxLength(32)])],
      documentType: ['',Validators.compose([Validators.required, Validators.maxLength(10)])]
    })

   }

  ngOnInit() {

    let id = this.activeRoute.snapshot.paramMap.get('id');
    console.log('ID Path', id);
    this.clientsService.getClientsById(id)
    .subscribe(res =>{
      console.log('get data ok', res);
      this.loadForm(res);
    })

  }

  saveClient(){
    console.log('Datos', this.clientFormGroup.value)
    this.clientsService.updateClient(this.clientFormGroup.value)
    .subscribe(res =>{
      console.log('Update ok', res);
      this.router.navigate(['/clients/clients-list'])
    }, error =>{
      console.error('Error', error);
    });
   }

   private loadForm(clients: IClient){
     this.clientFormGroup.patchValue({
      id: clients.id,
      name: clients.name,
      document: clients.document,
      email: clients.email,
      phoneNumber: clients.phoneNumber,
      documentType: clients.documentType
     });
   }

}
