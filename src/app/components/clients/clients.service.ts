import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {IClient } from './clients';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { createRequestParams } from 'src/app/utils/request.utils';

@Injectable({
  providedIn: 'root'
})
export class ClientsService {

  constructor( private http: HttpClient ) { }
  

  public queryClient(req?:any): Observable <IClient[]> {
    let params = createRequestParams(req);
    return this.http.get<IClient[]>(`${environment.END_POINT}/api/clients` , {params: params} )
    .pipe(map(res => {
      return res;
    }));
  }

  public saveClient(clients: IClient): Observable<IClient> {
    return this.http.post<IClient>(`${ environment.END_POINT }/api/clients`, clients)
    .pipe(map(res => {
      return res;
    }));
    }

public getClientsById(id: string){
  return this.http.get<IClient>(`${ environment.END_POINT }/api/clients/${id}`)
    .pipe(map(res => {
      return res;
    }));
}

public updateClient(clients: IClient): Observable<IClient>{
  return this.http.put<IClient>(`${ environment.END_POINT }/api/clients`,clients)
  .pipe(map(res => {
    return res;
  }));
}


public deleteItem(id: string): Observable<IClient> {
  return this.http.delete<IClient>(`${environment.END_POINT}/api/clients/${id}`)
  .pipe(map(res => {
    return res;
  }));
}

  }



