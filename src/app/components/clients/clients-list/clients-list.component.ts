import { Component, OnInit } from '@angular/core';
import { ClientsService } from './../clients.service'
import { IClient } from '../clients';

@Component({
  selector: 'app-clients-list',
  templateUrl: './clients-list.component.html',
  styleUrls: ['./clients-list.component.styl']
})
export class ClientsListComponent implements OnInit {

  public clientsList: IClient[];

  constructor(private clientsService: ClientsService ) { }

  ngOnInit() {
    this.clientsService.queryClient()
    .subscribe(res => {
      this.clientsList = res;
      console.log('response data', res);
    },
    error => console.error('error',error)
    );
  }

  deleteItem(id: string) {
    console.warn('ID ',id);
    this.clientsService.deleteItem(id)
    .subscribe(res => {
      console.warn('Item Deleted ok...', res);
      this.ngOnInit();
    }, error => console.warn('Error ',error));
  }

  

  saveClients(){
    console.log('Datos',)
  }



}
