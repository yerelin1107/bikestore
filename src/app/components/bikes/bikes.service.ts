import { Injectable } from '@angular/core';
import { IBike } from './interfaces/bike';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class BikesService {

  constructor( private http: HttpClient ) { }

  public queryBikes(): Observable<IBike[]>  {
    return this.http.get<IBike[]>(`${environment.END_POINT}/api/bikes`)
    .pipe(map(res => {
      return res;
    }));
  }
  /**
   * this...
   * @param IBike 
   * 
   */

  public saveBike(IBike: IBike): Observable<IBike> {
    return this.http.post<IBike>(`${ environment.END_POINT }/api/bikes`, IBike)
    .pipe(map(res => {
      return res;
    }));
  }

  /**
   * This method is for getting one IBike for id
   * @param id 
   */

  public getBikeById(id: String): Observable<IBike>{
    return this.http.get<IBike>(`${ environment.END_POINT }/api/bikes/${id}`)
    .pipe(map(res =>{
      return res;
    }));
  }

  /**
   * this... UPDATE
   * @param IBike 
   * 
   */


  public updateBike(IBike: IBike): Observable<IBike>{
    return this.http.put<IBike>(`${ environment.END_POINT }/api/bikes`, IBike)
    .pipe(map(res => {
      return res;
    }));
  }

  public getBikeBySerial(serial: string): Observable<IBike>{
    let params = new HttpParams();
    params = params.append('serial' ,serial);
    console.warn('PARAMS ',params);
    return this.http.get<IBike>(`${environment.END_POINT}/api/bikes/find-bike-by-serial`,{params: params})
      .pipe(map(res => {
        return res;

   }));
  }

  public deleteItem(id: string): Observable<IBike> {
    return this.http.delete<IBike>(`${environment.END_POINT}/api/bikes/${id}`)
    .pipe(map(res => {
      return res;
    }));
  }
}
