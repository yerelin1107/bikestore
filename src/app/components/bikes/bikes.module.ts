import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BikesListComponent } from './bikes-list/bikes-list.component';
import { BikesCreateComponent } from './bikes-create/bikes-create.component';
import { BikesUpdateComponent } from './bikes-update/bikes-update.component';
import { BikesViewComponent } from './bikes-view/bikes-view.component';
import { BikeRoutingModule } from './bikes-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MainBikesComponent } from './main-bikes/main-bikes.component'
import {AutoCompleteModule} from 'primeng/autocomplete';
import {InputSwitchModule} from 'primeng/inputswitch';
@NgModule({
  declarations: [
    BikesListComponent, 
    BikesCreateComponent, 
    BikesUpdateComponent, 
    BikesViewComponent, 
    MainBikesComponent],
  imports: [
    CommonModule,
    BikeRoutingModule,
    ReactiveFormsModule,
    AutoCompleteModule,
    InputSwitchModule
  ]
})
export class BikesModule {  }
