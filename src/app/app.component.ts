import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.styl']
})

export class AppComponent implements OnInit {
  title = 'Hello bikes-app';

numberOne: number = 36;
numberTwo: number = 10;
resultado1: number;
resultado2: number;
resultado3: number;
resultado4: number;
constructor() {}

ngOnInit(): void{
this.sumar();
this.restar();
this.multiplicar();
this.dividir();
}

private sumar(){
  this.resultado1 = this.numberOne + this.numberTwo;
  console.log('resultado', this.resultado1);
}

private restar(){
  this.resultado2 = this.numberOne - this.numberTwo;
  console.log('resultado',this.resultado2);
}
private multiplicar(){
  this.resultado3 = this.numberOne * this.numberTwo;
  console.log('resultado',this.resultado3);
}

private dividir(){
  this.resultado4 = this.numberOne / this.numberTwo;
  console.log('resultado',this.resultado4);
}

}
